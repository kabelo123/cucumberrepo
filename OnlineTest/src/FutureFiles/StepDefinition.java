package FutureFiles;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;



public class StepDefinition {
	WebDriver driver = null;
	@Given("^I navigate to zetaTek$")
	public void i_navigate_to_zetaTek() throws Throwable {
       System.setProperty("webdriver.chrome.driver","/home/keanan/Desktop/work/OnlineTest/chromedriver");
       driver = new ChromeDriver();
       driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
       driver.navigate().to("http://zetatek.info/");
	}

	@When("^i press the button$")
	public void i_press_the_button() throws Throwable {
		try {
			driver.findElement(By.id("homeButton")).click();	
		} catch (WebDriverException ex) {
		   System.out.println("Could not Navigate to Menu Screen");
		}
		
	}

	@Then("^it should navigate to Menu Page$")
	public void it_should_navigate_to_Menu_Page() throws Throwable {
      driver.findElement(By.cssSelector("#header > nav > ul > li:nth-child(4) > a")).click();
	}

@Then("^it should navigate to Contact$")
public void it_should_navigate_to_Contact() throws Throwable {
    driver.findElement(By.xpath("//*[@id='name']")).sendKeys("Kabelo");
    driver.findElement(By.cssSelector("#email")).sendKeys("kabelokgobane1@gmail.com");
    driver.findElement(By.cssSelector("#message")).sendKeys("This was sent using seleniumCucumber");


}

@Then("^it should navigate to Send an email$")
public void it_should_navigate_to_Send_an_email() throws Throwable {
	   driver.findElement(By.cssSelector("#contact > form > ul > li:nth-child(1) > input")).click();
	   System.out.println("Succesfully sent");

}

}
