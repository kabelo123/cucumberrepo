package CucumberR;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class CucumberStepRun {
	public static WebDriver driver;	
	@Given("^user is on Home Page$")
	public void user_is_on_Home_Page() throws Throwable {
		 System.setProperty("webdriver.chrome.driver","/home/keanan/Desktop/work/OnlineTest/chromedriver");
		    driver = new ChromeDriver();
	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	        driver.get("http://www.instagram.com");
	}

	@When("^user Navigate to LogIn Page$")
	public void user_Navigate_to_LogIn_Page() throws Throwable {
		driver.findElement(By.cssSelector("#react-root > section > main > article > div._kbq82 > div:nth-child(1) > div > form > span > button")).click();
			}

	@When("^user enters UserName and Password$")
	public void user_enters_UserName_and_Password() throws Throwable {
		driver.findElement(By.cssSelector("#email")).sendKeys("012345"); 
		driver.findElement(By.cssSelector("#pass")).sendKeys("0345566112"); 
	}

	@Then("^message displayed Login Successfully$")
	public void message_displayed_Login_Successfully() throws Throwable {
		driver.findElement(By.cssSelector("#loginbutton")).click(); 
		System.out.println("Login Successfully");
	}

	@When("^user LogOut from the Application$")
	public void user_LogOut_from_the_Application() throws Throwable {
		driver.findElement(By.cssSelector("body > div:nth-child(12) > div > button")).click();
		driver.findElement(By.cssSelector("#react-root > section > nav > div._s4gw0._1arg4 > div > div > div._devkn > div > div:nth-child(3) > a")).click();
		driver.findElement(By.cssSelector("#react-root > section > main > article > header > section > div._ienqf > div > button")).click();
		
	}

	@Then("^message displayed Logout Successfully$")
	public void message_displayed_Logout_Successfully() throws Throwable {
		driver.findElement(By.cssSelector("body > div:nth-child(14) > div > div._o0j5z > div > ul > li:nth-child(4) > button")).click();
        System.out.println("LogOut Successfully");
	}
}
