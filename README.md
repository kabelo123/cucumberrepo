# CucumberRepo project has 2 programs uploaded namely CucumberR & FutureFiles
# CucumberR Navigates through the zetatek.info website to send a message/email
# FutureFiles Login instagram.com using your facebook account and then logs out.

#Dependencies
you will need eclipse ide to run the program and the following jar files to be added in the external Jar files.

#Jar files needed.
- selenium
- cucumber-core-1.2.5.jar
- cucumber-html-0.2.6.jar
- cucumber-java-1.2.5.jar
- cucumber-junit-1.2.5.jar
- cucumber-jvm-deps-1.0.5.jar
- gherkin-2.12.2.jar
- hamcrest-all-1.3.jar
- selenium-server-standalone-3.11.0.jar

After adding all of these jar files goto your src folder.

#functionality of CucumberR

- Goto->src->CucumberR->CucumberstepRun.feature

Right click in the feature folder and select RunAs->CucumberFeature
select the CucumberstepRun.java class to view the source code.

#functionality of FutureFiles

- Goto->src->Futurefiles->FFStandAlone.feature

Right click in the feature folder and select RunAs->CucumberFeature
Select the StepDefinition.java class to view the source code.

##LETS DO THIS...

